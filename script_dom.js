function sendMessage() {
    var msg = document.getElementById("chatbox").value;
    var msgArea = document.getElementById("chatarea");

    var msgDiv = document.createElement("div");
    //Estilo div mensagens
    msgDiv.style.backgroundColor = 'hsl(0, 0%, 98%)'
    msgDiv.style.borderStyle = 'solid'
    msgDiv.style.borderWidth = '1'
    msgDiv.style.padding = '1rem'
    msgDiv.style.fontSize = '18'
    msgDiv.style.display = 'flex'
    var msgText = document.createElement('p');
    msgText.textContent = msg
    //Estilo texto
    msgText.style.width = '99%'

    var buttonDiv = document.createElement("div");
    buttonDiv.style.marginLeft = '85%'
    //Botao editar
    var editButton = document.createElement("button");
    editButton.style.backgroundColor = 'hsl(29, 89%, 66%)'
    editButton.innerHTML = "Editar";
    editButton.onclick = function() {
        var novaMsg = prompt("Edite sua mensagem:", msg);
        if (novaMsg != null) {
            msgText.textContent = novaMsg;
        }
    };

    //Botao deletar
    var deleteButton = document.createElement("button");
    deleteButton.style.backgroundColor = 'hsl(0, 89%, 66%)'
    deleteButton.innerHTML = "Deletar";
    deleteButton.onclick = function() {
        msgArea.removeChild(msgDiv);
        msgArea.removeChild(buttonDiv);
    };

    msgDiv.appendChild(msgText);
    buttonDiv.appendChild(editButton);
    buttonDiv.appendChild(deleteButton);

    msgArea.appendChild(msgDiv);
    msgArea.appendChild(buttonDiv);
    //document.getElementById("messageInput").value = "";
}


